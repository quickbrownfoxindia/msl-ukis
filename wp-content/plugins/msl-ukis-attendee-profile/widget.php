<?php

/**
* Adds UKIS Attendee Profile widget
*/
class Ukisattendeeprofile_Widget extends WP_Widget {

	/**
	* Register widget with WordPress
	*/
	function __construct() {
		parent::__construct(
			'ukisattendeeprofile_widget', // Base ID
			esc_html__( 'UKIS Attendee Profile', 'textdomain' ) // Name
		);
	}

	/**
	* Widget Fields
	*/
	private $widget_fields = array(
	);

	/**
	* Front-end display of widget
	*/
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		// Output generated fields
		// 
		if(!function_exists('um_get_display_name')) {
			echo "Plugin requires Ultimate Members plugin. Please install it before using this plugin.";
			echo $args['after_widget'];
			return;
		}
		global $ultimatemember;
// 		print_r($ultimatemember); die;

// 		$user_id = get_current_user_id();

		// Set user ID
// 		$ultimatemember->user->set( $user_id );

		// Returns current user avatar
		$avatar_uri = um_get_avatar_uri( um_profile('profile_photo'), 32 );

		// Returns default UM avatar, e.g. https://ultimatemember.com/wp-content/uploads/2015/01/default_avatar.jpg
		$default_avatar_uri = um_get_default_avatar_uri();
		echo '<div style="word-wrap: break-word">';
		echo '<img src="'.$default_avatar_uri.'" />';
		echo '<p>'.um_get_display_name().'</p>';
		
		echo "</div>";
		echo $args['after_widget'];
	}

	/**
	* Back-end widget fields
	*/
	public function field_generator( $instance ) {
		$output = '';
		foreach ( $this->widget_fields as $widget_field ) {
			$widget_value = ! empty( $instance[$widget_field['id']] ) ? $instance[$widget_field['id']] : esc_html__( $widget_field['default'], 'textdomain' );
			switch ( $widget_field['type'] ) {
				default:
					$output .= '<p>';
					$output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'textdomain' ).':</label> ';
					$output .= '<input class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" type="'.$widget_field['type'].'" value="'.esc_attr( $widget_value ).'">';
					$output .= '</p>';
			}
		}
		echo $output;
	}

	public function form( $instance ) {
		$this->field_generator( $instance );
	}

	/**
	* Sanitize widget form values as they are saved
	*/
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		foreach ( $this->widget_fields as $widget_field ) {
			switch ( $widget_field['type'] ) {
				case 'checkbox':
					$instance[$widget_field['id']] = $_POST[$this->get_field_id( $widget_field['id'] )];
					break;
				default:
					$instance[$widget_field['id']] = ( ! empty( $new_instance[$widget_field['id']] ) ) ? strip_tags( $new_instance[$widget_field['id']] ) : '';
			}
		}
		return $instance;
	}
} // class Ukisattendeeprofile_Widget

// register UKIS Attendee Profile widget
function register_ukisattendeeprofile_widget() {
	register_widget( 'Ukisattendeeprofile_Widget' );
}
add_action( 'widgets_init', 'register_ukisattendeeprofile_widget' );