# Show feeds for form
* Ever wondered what type of feeds are setup for a form? what forms have what feeds e.g. Does this form have a Podio feed?
* Whether my sites or customer sites, it becomes increasingly tedious to have to go through each form, visit the settings page for each available add-on to see 
if there's a feed for that add-on. [count the number of clicks] It becomes quicker for me to just open up the database and look at the feeds table
* Here's what I said to myself: I would love if at a glance, when I visit the forms list, I can instantly see the feeds that are setup for each form
Bonus points if I can also see whether they are active or not
* My wish is my command ;-) 
* Design decisions: always looking to meld into the current interface as much as possible. It shouldn't look out of place
    * another column: mmm, that makes things get messy real quick. Saw this with my Stripe plugin
    * when you hover over a form row and the Settings menu, it shows you each add-on, could possibly place an icon there.
    Unobtrusive, nice, but doesn't meet our goal of at-a-glance. Trying to reduce the # of steps it takes to get to the
    information I want
    * Not the prettiest, but show right underneath the form title. So when you visit the page, you can see right away
    which feeds are available for a form.
    * Bonus, grayed out icon if inactive, bolded icon if active
* UI expert review & comments
* And now it's yours, in our thank-you to Gravity Forms, Free forever Gravity Forms Utility plugin
* see Trello card.

# Marketing
* The #gravityforms Utility plugin just keeps getting better-and-better.
* This. is. saving. me. so. much. time. with. #gravityforms
* This has been bugging me in #gravityforms for over a year. So I finally did something about it.
* I had enough of not being able to see at-a-glance which feeds were connected to which #gravityforms. So I did something about it.
* Now you can quickly answer the question: Which forms have this type of feed? #gravityforms #alldayeveryday
* Now you can quickly answer the question: Does this form have this type of feed? #gravityforms #alldayeveryday
* #gravityforms itch. scratched.
* the days of asking which one of my forms is connected to ___[Podio] [add-on] are over!
* Just added something that is *huge* for me to the #gravityforms Utiliy plugin. No more searching around! animated gif

* animated gif as main image