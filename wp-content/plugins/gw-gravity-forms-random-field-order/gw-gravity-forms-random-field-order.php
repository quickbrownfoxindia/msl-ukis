<?php
/**
 * Gravity Wiz // Gravity Forms // Random Fields
 *
 * Randomly display a specified number of fields on your form.
 *
 * @version 1.0
 * @author  David Smith <david@gravitywiz.com>
 * @license GPL-2.0+
 * @link    https://gravitywiz.com/random-fields-with-gravity-forms/
 *
 * Plugin Name: Gravity Forms Random Fields
 * Plugin URI:  https://gravitywiz.com/random-fields-with-gravity-forms/
 * Description: Randomly display a specified number of fields on your form.
 * Author:      Gravity Wiz
 * Version:     1.0
 * Author URI:  http://gravitywiz.com
 */
class GFRandomFields {
    
    public $all_random_field_ids;
    public $display_count;
    public $selected_field_ids = array();
    
    function __construct($form_id, $display_count = 5, $random_field_ids = false) {

        $this->all_random_field_ids = (array) $random_field_ids;
        $this->display_count = $display_count;
        
        add_filter("gform_pre_render_$form_id", array(&$this, 'pre_render'));
        add_filter("gform_form_tag_$form_id", array(&$this, 'store_selected_field_ids'));
        add_filter("gform_validation_$form_id", array(&$this, 'validate'));
        
    }
    
    function pre_render($form) {
        return $this->filter_form_fields($form, $this->get_selected_field_ids());
    }
    
    function store_selected_field_ids($form_tag) {
    	$hash  = $this->get_selected_field_ids_hash();
        $value = implode(',', $this->get_selected_field_ids());
        $input = sprintf( '<input type="hidden" value="%s" name="gfrf_field_ids_%s">', $value, $hash );
        return $form_tag . $input;
    }
    
    function validate($validation_result) {
        
        $validation_result['form'] = $this->filter_form_fields($validation_result['form'], $this->get_selected_field_ids());
        $validation_result['is_valid'] = true;
        
        foreach($validation_result['form']['fields'] as $field) {
            if($field['failed_validation'])
                $validation_result['is_valid'] = false;
        }
        
        return $validation_result;
    }
    
    function filter_form_fields($form, $selected_fields) {
        
        $filtered_fields = array();
        
        foreach($form['fields'] as $field) {
            
            if(in_array($field['id'], $this->all_random_field_ids)) {
                if(in_array($field['id'], $selected_fields))
                    $filtered_fields[] = $field;
            } else {
                $filtered_fields[] = $field;
            }
            
        }
        
        $form['fields'] = $filtered_fields;
        
        return $form;
    }
    
    function get_selected_field_ids() {
        
        // check if class has already init fields
        if( ! empty( $this->selected_field_ids ) ) {
	        return $this->selected_field_ids;
        }

	    $hash = $this->get_selected_field_ids_hash();

	    // check if fields have been submitted
        $field_ids = rgpost( "gfrf_field_ids_{$hash}" );
        if( ! empty( $field_ids ) ) {
	        return explode( ',', $field_ids );
        }
        
        $field_ids = array();
        $keys      = array_rand( $this->all_random_field_ids, $this->display_count );

	    if( ! is_array( $keys ) ) {
		    $keys = array( $keys );
	    }

        foreach( $keys as $key ) {
            $field_ids[] = $this->all_random_field_ids[ $key ];
        }
        
        $this->selected_field_ids = $field_ids;
        
        return $field_ids;
    }

    public function get_selected_field_ids_hash() {
	    return wp_hash( implode( '_', $this->all_random_field_ids ) );
    }
    
}

new GFRandomFields( 167, 2, array( 1, 2, 3, 4, 5 ) );

new GFRandomFields( 167, 2, array( 8, 9, 10 ) );