<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://mslgroup.tech
 * @since             1.0.0
 * @package           Msl_Info_Box
 *
 * @wordpress-plugin
 * Plugin Name:       Info Box
 * Plugin URI:        https://mslgroup.tech
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            MSL DEV TEAM
 * Author URI:        https://mslgroup.tech
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       msl-info-box
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// register My_Widget
add_action( 'widgets_init', function() {
    require "widget.php";
	register_widget( 'Infobox_Widget' );
});
