<?php
/*
Plugin Name: Unique Id Field for Gravity Forms
Description: Generate a hidden unique id field in gravity forms
Version: 1.2
Author: Amy Hill
Author URI: http://anvilzephyr.com
License: GPL-2.0+
Copyright: 2017 Anvil Zephyr LLC 
           Plugin may be modified but not redistributed. 
           Author information must remain.
*/


add_action( 'gform_loaded', array( 'GF_Unique_Id_Field_Bootstrap', 'load' ), 5 );

class GF_Unique_Id_Field_Bootstrap {

	public static function load() {

		if ( ! method_exists( 'GFForms', 'include_payment_addon_framework' ) ) {

			return;
		}

		require_once((__DIR__). '/class-gf-unique-id-field.php' );
		
	}

}
function gf_unique_id_field() {
	return AZGFUnique\GFUniqueIdField::get_instance();
}