<?php
namespace AZGFUnique;
if ( ! class_exists( '\GFForms' ) ) {
	die();
}


if (!class_exists('GFUniqueIdField')){
	
class GFUniqueIdField extends \GF_Field {

	public $type = 'az_unique_id';
   	private $slug = 'gfuniqueid';

	
	public function get_form_editor_field_title() {
		return 'Unique ID';
	}
	
	public function get_form_editor_field_settings() {
		return array(
			'conditional_logic_field_setting',
			'label_setting',
			'rules_setting',
			'description_setting',
		);
	}

	public function get_form_editor_button()
    {
        return array(
            'group' => 'advanced_fields',
            'text'  => 'Unique ID'
        );
    }

	
   /**
	 * populate the value
	 * @return string unique id
	 */
	public function get_value_submission( $field_values, $get_from_post_global_var = true ) {
      
      // Filter prefix
      $prefix = apply_filters('az_gf_uid_filter','UKIS18');
      return apply_filters('az_gf_value_filter',$this->generate_unique_id($prefix));

	}
   
   /**
    * Custom function to generate unique id
    * @global type $wpdb
    * @param string $prefix
    * @return string
    */
   private function generate_unique_id($prefix){
      
      $uid = $prefix . mt_rand(1000, 9999);
      // global $wpdb;
      // $table = \GFFormsModel::get_lead_details_table_name();
      // if ($wpdb->get_var($wpdb->prepare("SELECT value from $table where value=%s",$uid)))
      //    $this->generate_unique_id($prefix);
      return $uid;
      
   }

	public function get_field_input( $form, $value = '', $entry = null ) {

		$is_entry_detail = $this->is_entry_detail();
		$is_form_editor  = $this->is_form_editor();
		$form_id  = $form['id'];
		$id       = intval( $this->id );
		$field_id = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";
		$form_id  = ( $is_entry_detail || $is_form_editor ) && empty( $form_id ) ? rgget( 'id' ) : $form_id;
      $field_type         = 'hidden';
		$class_attribute    = $is_entry_detail || $is_form_editor ? '' : "class='gform_hidden'";
		$required_attribute = $this->isRequired ? 'aria-required="true"' : '';
      $invalid_attribute     = $this->failed_validation ? 'aria-invalid="true"' : 'aria-invalid="false"';
		$disabled_text = $is_form_editor ? "disabled='disabled'" : '';
		$class_suffix  = $is_entry_detail ? '_admin' : '';
		/*--------------- Field ---------------*/
		return sprintf( "<input name='input_%d' id='%s' type='$field_type' data-zone='azu' {$class_attribute} {$required_attribute} {$invalid_attribute} value='%s' %s/>", $id, $field_id, esc_attr( $value ), $disabled_text );
	}
   
   public function get_field_content( $value, $force_frontend_label, $form ) {
		$form_id         = $form['id'];
		$admin_buttons   = $this->get_admin_buttons();
		$is_entry_detail = $this->is_entry_detail();
		$is_form_editor  = $this->is_form_editor();
		$is_admin        = $is_entry_detail || $is_form_editor;
		$field_label     = $this->get_field_label( $force_frontend_label, $value );
		$field_id        = $is_admin || $form_id == 0 ? "input_{$this->id}" : 'input_' . $form_id . "_{$this->id}";
		$field_content   = ! $is_admin ? '{FIELD}' : $field_content = sprintf( "%s<label class='gfield_label' for='%s'>%s</label>{FIELD}", $admin_buttons, $field_id, esc_html( $field_label ) );

		return $field_content;
	}

	

}
\GF_Fields::register( new GFUniqueIdField() );

}