There are two filters:
az_gf_value_filter which changes the entire value
az_gf_uid_filter which changes the prefix

Examples:

add_filter('az_gf_value_filter','change_value_to_timestamp');

function change_value_to_timestamp($value){
   return current_time('timestamp');
}

add_filter('az_gf_uid_filter','change_prefix');

function change_prefix($value){
   return 'My_Prefix_';
}