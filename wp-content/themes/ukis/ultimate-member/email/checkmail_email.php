<p>&nbsp;</p>
<p>&nbsp;</p>
<div><style>

</style></div>
<p>&nbsp;</p>
<div style="width: 600px; margin: 0 auto; background: #fff;">
<div><a href="https://destinationuttarakhand.in/" target="_blank" rel="noopener"><img src="https://beta.destinationuttarakhand.in/wp-content/uploads/2018/08/header.jpg" alt="" /></a></div>
<div style="padding: 15px 30px; font-size: 14px;">
<p>Dear User,</p>
<p>Thank you for registering with us. To complete the registration process, please log in by clicking on the link below:</p>
<p><a href="{account_activation_link} " target="_blank" rel="noopener">Activate Account</a></p>
<p>This registration link expires in a day for security purposes.</p>
<p>Welcome to the Uttarakhand Investors Summit 2018</p>
<p>Regards</p>
</div>
<div style="border-top: 2px solid #352c80; margin: 0 30px;">
<div style="display: inline;">
<p style="color: #352c80; display: inline-block; font-weight: 600; font-size: 12px;">Follow Us</p>
<a href="https://www.facebook.com/Destination-Uttarakhand-241171896696094/" target="_blank" rel="noopener"><img src="https://beta.destinationuttarakhand.in/wp-content/uploads/2018/08/facebook.png" alt="footer" /></a> <a href="https://twitter.com/DestinationUKIS" target="_blank" rel="noopener"><img src="https://beta.destinationuttarakhand.in/wp-content/uploads/2018/08/twitter.png" alt="footer" /></a></div>
<div style="display: inline; float: right;">
<p style="color: #352c80; font-weight: 600; font-size: 12px;"><a style="text-decoration: none;" target="">#DestinationUttarakhand</a></p>
</div>
</div>
</div>