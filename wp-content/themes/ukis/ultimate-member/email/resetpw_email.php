<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title></title>
</head>
<style>

</style>
<body style="font-family:'Helvetica','Arial', sans-serif !important;background: #e5e5e5;">
	<div style="width:600px;margin:0 auto;background: #fff;">
		<div >
			<a href="https://destinationuttarakhand.in/" target="_blank"><img src="https://beta.destinationuttarakhand.in/wp-content/uploads/2018/08/header.jpg" alt=""/></a>
		</div>
		<div style="padding:15px 30px;font-size:14px;">
			<p style="">Dear User,</p>
			<p style="">We received a request to reset the password for your account. If you made this request, click the link below to change your password:</p>
			<p style="">
				<a href="{password_reset_link}" target="_blank">Reset Password</a>
			</p>
			<p style="">Regards</p>
		</div>

		<div style="border-top:2px solid #352c80;margin:0 30px;">
			<div style="display:inline">
				<p style="color:#352c80;display:inline-block;font-weight: 600;font-size:12px;">Follow Us</p>
				<a href="https://www.facebook.com/Destination-Uttarakhand-241171896696094/" target="_blank"><img src="https://beta.destinationuttarakhand.in/wp-content/uploads/2018/08/facebook.png" alt="footer"/></a>
				<a href="https://twitter.com/DestinationUKIS" target="_blank"><img src="https://beta.destinationuttarakhand.in/wp-content/uploads/2018/08/twitter.png" alt="footer"/></a>
			</div>
			<div style="display:inline;float:right">
				<p style="color:#352c80;font-weight: 600;font-size:12px;">
					<a href="" target="" style="text-decoration:none;">#DestinationUttarakhand</a>
				</p>
			</div>
		</div>
	</div>
</body>
</html>