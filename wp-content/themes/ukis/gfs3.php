<?php

/**
 *	Send Gravity Forms file uploads to Amazon S3
 *	@author Ren Ventura <EnageWP.com>
 *	@link http://www.engagewp.com/send-gravity-forms-file-uploads-to-amazon-s3/
 */

//* Include the required library
include_once 'inc/S3.php';

//* AWS access info
define( 'awsAccessKey', 'AKIAIMGKWSC54XHK7LUA' );
define( 'awsSecretKey', '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' );
define( 'GFS3_BUCKET', 'destinationuttarakhand' );

//* Form constants
define( 'FORM_ID', 1 );
define( 'FILE_UPLOAD_FIELD_ID', 18 );

//* Upload the file after form is submitted (Product Edit)
add_action( 'gform_after_submission', 'gf_submit_to_s3', 10, 2 );
function gf_submit_to_s3( $entry, $form ) {

	foreach($form['fields'] as $field) {
		if($field['type'] == 'fileupload') {

			send_to_s3($field['id'], $field['label'], $entry, $form);

		}
	}
}

function send_to_s3($field_id, $field_label, $entry, $form) {

// echo "<pre>";	print_r($form); die;

	// Bail if there is no file uploaded to the form
	if ( empty( $entry[$field_id] ) )
		return;

	// Instantiate the S3 class
	$s3 = new S3( awsAccessKey, awsSecretKey );

	// Get the URL of the uploaded file
	$file_url = $entry[$field_id];

	// Retreive post variables
	$file_name = $_FILES['input_' . $field_id]['name'];

	$file_parts = pathinfo($file_name);

// echo $path_parts['dirname'], "\n";
// echo $path_parts['basename'], "\n";
// echo $path_parts['extension'], "\n";
// echo $path_parts['filename'], "\n"; // since PHP 5.2.0


	/**
	 *	File Permissions
	 *
	 *	ACL_PRIVATE
	 *	ACL_PUBLIC_READ
	 *	ACL_PUBLIC_READ_WRITE
	 *	ACL_AUTHENTICATED_READ
	 */

	// Create a new bucket if it does not exist (happens only once)
	$s3->putBucket( GFS3_BUCKET, S3::ACL_AUTHENTICATED_READ );

	// Parse the URL of the uploaded file
	$url_parts = parse_url( $file_url );

	// Full path to the file
	$full_path = $_SERVER['DOCUMENT_ROOT'] . $url_parts['path'];


	// Add the file to S3
	$s3->putObjectFile( $full_path, GFS3_BUCKET, 'uploads/'. get_current_user_id() . '/' . slugify(strtolower($form['title'])) . '/' . slugify(strtolower($field_label)) . '/' . $file_parts['filename'] .'-'. gmdate('Ymdhis') . '.' . $file_parts['extension'], S3::ACL_AUTHENTICATED_READ );

	// Confirmation/Error
	if ( $s3->putObjectFile( $full_path, GFS3_BUCKET, 'uploads/'. get_current_user_id() . '/' . slugify(strtolower($form['title'])) . '/' . slugify(strtolower($field_label)) . '/' . $file_parts['filename'] .'-'. gmdate('Ymdhis') . '.' . $file_parts['extension'], S3::ACL_AUTHENTICATED_READ ) ) {
// 	    printf( 'Your file <strong>%1$s</strong> was successfully uploaded.', $file_name );
	} else {
	    wp_die( __( 'It looks like something went wrong while uploading your file. Please try again. If you continue to experience this problem, please contact the site administrator.' ) );
	}


}

function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}
