<?php
/**
 * UKIS functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package UKIS
 */

 /**
  * Theme init
  */
 add_action('after_setup_theme', 'uncode_language_setup');
 function uncode_language_setup() {
 	load_child_theme_textdomain('uncode', get_stylesheet_directory() . '/languages');
 }

 /**
  * Enqueue scripts and styles.
  */
 function theme_enqueue_styles() {
 	$production_mode = ot_get_option('_uncode_production');
 	$resources_version = ($production_mode === 'on') ? null : rand();
 	$parent_style = 'uncode-style';
 	$child_style = array('uncode-custom-style');
 	wp_enqueue_style($parent_style, get_template_directory_uri() . '/library/css/style.css', array(), $resources_version);

 	wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', $child_style, $resources_version);

	wp_enqueue_script('ukis-navigation', get_stylesheet_directory_uri() . '/dist/js/navigation.js', array(), '20151215', true);
	wp_enqueue_script('ukis-skip-link-focus-fix', get_stylesheet_directory_uri() . '/dist/js/skip-link-focus-fix.js', array(), '20151215', true);
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/dist/js/main.js', array('jquery'), null, true);
	 
	/* tooltip js */
	wp_enqueue_script('tooltipster', 'https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/js/jquery.tooltipster.min.js', array('jquery'), null, true);
	 
 	wp_enqueue_style('tooltipster', 'https://cdnjs.cloudflare.com/ajax/libs/tooltipster/3.3.0/css/tooltipster.min.css', [], null);

 }
 add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

/**
* Enables the HTTP Strict Transport Security (HSTS) header.
  *
  * @since 1.0.0
  */
function tgm_io_strict_transport_security() {
  header('Strict-Transport-Security: max-age=10886400; includeSubDomains; preload');
}
add_action('send_headers', 'tgm_io_strict_transport_security');


/* custom code */
/* increase size */
add_filter( 'gform_chainedselects_max_file_size', 'set_size' );
function set_size( $size ) {
    return 20000000; //~20mb
}


/* unique id */
/* delegate form (id: 1) */
add_action('gform_after_submission_1', 'nk_add_submission_id', 10, 2);
/**
 * Assign unique ID to the Service & Support form, for Request Number
 */
function nk_add_submission_id($entry, $form) {
	global $wpdb;
	$field_number = 42;
	$SubmissionID = 'S-' . $entry['id'];

	$wpdb->insert("{$wpdb->prefix}rg_lead_detail", array(
		'value'			=> $SubmissionID,
		'field_number'	=> $field_number,
		'lead_id'		=> $entry['id'],
		'form_id'		=> $entry['form_id']
	));
}


function get_current_user_role() {
global $wp_roles;
$current_user = wp_get_current_user();
$roles = $current_user->roles;
echo '<div class="heading-text el-text"><h2 class="h5 text-color-jevc-color"><span>You are registering as '. ($roles? ucfirst(str_replace('um_','', $roles[0])) : '') .'</span></div>';
}

add_shortcode( 'show-rsvp-role', 'get_current_user_role' );

require_once "gfs3.php";


/* custom error message for sticky forms list plugin */

add_shortcode( 'stickylist-wrapper', 'stickylist_wrapper_shortcode_handler' );
function stickylist_wrapper_shortcode_handler($atts) {
	
	$shortcode_id = shortcode_atts( array(
                'id'            => '1',
                'user'          => '',
                'showto'        => '',
                'field'         => '',
                'value'         => '',
                'test'          => ''
            ), $atts );
	
	$form_id = $shortcode_id['id'];
	
	$output =  do_shortcode( "[stickylist id='" . $form_id . "']");
	/* default message length is 67 charaters */
	if(strlen($output) == 67) {
		echo '<p><a href="'.get_site_url(null, '/rsvp').'">No submission found. Click here to start your submission.</a></p>';
	} else {
		echo $output;
	}
}